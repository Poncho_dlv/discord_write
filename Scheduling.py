#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

import discord
import pyshorteners
from pytz import timezone

import discord_resources
from discord_resources.Utilities import DiscordUtilities
from spike_database.Contests import Contests
from spike_database.DiscordGuild import DiscordGuild
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Contest import Contest
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from spike_user_db.Users import Users
from spike_utilities.Statistics import Statistics
from spike_utilities.Utilities import Utilities


class Scheduling:

    def __init__(self, client):
        self.__client = client

    async def write(self, ctx, matches, competition, platform_id, output_option):
        utils = DiscordUtilities(self.__client)
        settings = DiscordGuildSettings(f"{ctx.guild.id}")
        channel = ctx.message.channel
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        user_db = Users()
        contest_db = Contests()
        rsrc = discord_resources.Resources()
        output_msg = ""
        write_competition = True
        competition_url = "https://spike.ovh/competition?competition_id={}&platform_id={}".format(competition.get_id(), platform_id)
        s = pyshorteners.Shortener(timeout=4)
        try:
            url = s.dagd.short(competition_url)
        except:
            url = competition_url

        for match in matches:
            match = Contest(match)
            if match.get_coach_home().get_id() is not None or match.get_coach_away().get_id() is not None:
                current_competition = match.get_competition_name()
                competition_id = db.get_competition_id(current_competition, platform_id)
                if competition_id is not None:
                    competition_data = db.get_competition_data(competition_id)
                    if competition_data is not None:
                        output_msg += "\n"
                        if write_competition:
                            write_competition = False

                            round_display = Utilities.get_round_label(match.get_format(), match.get_round(), competition.get_rounds_count(), settings.get_language())
                            output_msg += "\n"
                            output_msg += "{} __**{} - {}**__ {}".format(competition_data[5], current_competition, round_display, competition_data[5])
                            output_msg += "<{}>".format(url)
                            output_msg += "\n"

                        contest_db.add_or_update_contest(match, platform_id, competition.get_rounds_count(), competition_data[5])

                        async def generate_coach_label(coach):
                            coach_name = coach.get_name()
                            if coach.get_id() is None:
                                coach_name = Translator.tr("#_rank_writer.a_i", settings.get_language())
                            elif output_option["mention_coach"]:
                                user_linked = user_db.get_user_linked(coach.get_id(), platform_id)
                                if user_linked is not None:
                                    user_linked = user_db.get_user(str(user_linked))
                                    if ctx.guild:
                                        discord_member = ctx.guild.get_member(user_linked.get("discord_id"))
                                        if discord_member is not None:
                                            discord_member = await ctx.guild.fetch_member(user_linked.get("discord_id"))
                                        if discord_member is not None:
                                            coach_name = discord_member.mention

                            return coach_name

                        coach1 = await generate_coach_label(match.get_coach_home())
                        logo1 = common_db.get_team_emoji(match.get_team_home().get_logo())
                        coach2 = await generate_coach_label(match.get_coach_away())
                        logo2 = common_db.get_team_emoji(match.get_team_away().get_logo())

                        if output_option["display_team_name"]:
                            team1 = match.get_team_home().get_name()
                            team2 = match.get_team_away().get_name()

                            output_msg += "**{}** :: {} {} **vs** {} {} :: **{}**".format(coach1, team1, logo1, logo2, team2, coach2)
                        else:
                            output_msg += "{} {} vs {} {}".format(coach1, logo1, logo2, coach2)

                        if output_option["display_date"]:
                            match_date = contest_db.get_contest_date(match.get_contest_id(), platform_id)
                            if match_date is None:
                                output_msg += " {} *Not planned yet*".format(rsrc.get_calendar_emoji())
                            else:
                                try:
                                    if "??:??" in match_date:
                                        match_date = datetime.strptime(match_date, "%Y-%m-%d ??:??")
                                        output_format = "%d/%m :: ??:??"
                                    elif ":??" in match_date:
                                        match_date = datetime.strptime(match_date, "%Y-%m-%d %H:??")
                                        output_format = "%d/%m :: %H:??"
                                    else:
                                        match_date = datetime.strptime(match_date, "%Y-%m-%d %H:%M")
                                        output_format = "%d/%m :: %H:%M"

                                    input_tz = timezone("UTC")
                                    match_date = input_tz.localize(match_date, True)
                                    output_tz = timezone(settings.get_timezone())
                                    match_date = match_date.astimezone(output_tz).strftime(output_format)

                                    output_msg += " {} *{} {}* ".format(rsrc.get_calendar_emoji(), match_date, settings.get_timezone())
                                except:
                                    output_msg += " {} *Not planned yet*".format(rsrc.get_calendar_emoji())

                        elif output_option["display_bet"]:
                            emojis = None
                            if match.get_coach_home().get_id() is not None and match.get_coach_away().get_id() is not None:
                                home_odd, draw_odd, away_odd = Statistics.get_contest_odds(match, platform_id)
                                output_msg += "\n``{} - {} - {}``".format(home_odd, draw_odd, away_odd)
                                emojis = ["🏠", "🇽", "🚌"]
                            await utils.send_custom_message(channel, output_msg, emoji=emojis)
                            output_msg = ""
                        elif len(output_msg) >= 1800:
                            await utils.send_custom_message(channel, output_msg)
                            output_msg = ""
        if len(output_msg) > 0:
            await utils.send_custom_message(channel, output_msg)

    async def write_calendar_mode(self, ctx, matches, competition, platform_id, output_option):
        utils = DiscordUtilities(self.__client)
        settings = DiscordGuildSettings(f"{ctx.guild.id}")
        channel = ctx.message.channel
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        user_db = Users()
        contest_db = Contests()
        write_competition_title = True
        round_display = ""

        for match in matches:
            match = Contest(match)
            if match.get_coach_home().get_name() is not None or match.get_coach_away().get_name() is not None:
                current_competition = match.get_competition_name()
                competition_id = db.get_competition_id(current_competition, platform_id)
                if competition_id is not None:
                    competition_data = db.get_competition_data(competition_id)
                    if competition_data is not None:
                        if write_competition_title:
                            write_competition_title = False
                            round_display = Utilities.get_round_label(match.get_format(), match.get_round(), competition.get_rounds_count(), settings.get_language())
                            main_title = " {} __**{} - {}**__ {} ".format(competition_data[5], current_competition, round_display, competition_data[5])
                            main_title = main_title.center(150, " ")
                            output = "".ljust(50, "=")
                            output += "\n"
                            output += main_title
                            output += "\n"
                            output += "".ljust(50, "=")
                            await utils.send_custom_message(channel, output)

                        competition_title = "{} - {}".format(current_competition, round_display)
                        competition_url = "https://spike.ovh/competition?competition_id={}&platform_id={}".format(competition_data[1], platform_id)

                        async def generate_coach_label(coach):
                            coach_name = coach.get_name()
                            if coach.get_id() is None:
                                coach_name = Translator.tr("#_rank_writer.a_i", settings.get_language())
                            elif output_option["mention_coach"]:
                                user_linked = user_db.get_user_linked(coach.get_id(), platform_id)
                                if user_linked is not None:
                                    user_linked = user_db.get_user(str(user_linked))
                                    if ctx.guild:
                                        discord_member = ctx.guild.get_member(user_linked.get("discord_id"))
                                        if discord_member is not None:
                                            discord_member = await ctx.guild.fetch_member(user_linked.get("discord_id"))
                                        if discord_member is not None:
                                            coach_name = discord_member.mention
                            return coach_name

                        coach1 = await generate_coach_label(match.get_coach_home())
                        logo1 = common_db.get_team_emoji(match.get_team_home().get_logo())
                        coach2 = await generate_coach_label(match.get_coach_away())
                        logo2 = common_db.get_team_emoji(match.get_team_away().get_logo())

                        if output_option.get("display_team_name"):
                            team1 = match.get_team_home().get_name()
                            team2 = match.get_team_away().get_name()

                            match_title = "**{}** :: {} {} **vs** {} {} :: **{}**".format(coach1, team1, logo1, logo2, team2, coach2)
                        else:
                            match_title = "{} {} vs {} {}".format(coach1, logo1, logo2, coach2)

                        match_date = contest_db.get_contest_date(match.get_contest_id(), platform_id)
                        if match_date is None:
                            # /* Not planned yet */
                            match_description = Translator.tr("#_schedule_command.not_planed", settings.get_language())
                        else:
                            try:
                                if "??:??" in match_date:
                                    match_date = datetime.strptime(match_date, "%Y-%m-%d ??:??")
                                    output_format = "%d/%m :: ??:??"
                                elif ":??" in match_date:
                                    match_date = datetime.strptime(match_date, "%Y-%m-%d %H:??")
                                    output_format = "%d/%m :: %H:??"
                                else:
                                    match_date = datetime.strptime(match_date, "%Y-%m-%d %H:%M")
                                    output_format = "%d/%m :: %H:%M"

                                input_tz = timezone("UTC")
                                match_date = input_tz.localize(match_date, True)
                                output_tz = timezone(settings.get_timezone())
                                match_date = match_date.astimezone(output_tz).strftime(output_format)

                                match_description = "{} {}".format(match_date, settings.get_timezone())
                            except:
                                match_description = Translator.tr("#_schedule_command.not_planed", settings.get_language())

                        embed = discord.Embed(title=match_title, description=match_description, color=0x992d22)
                        embed.set_author(name=competition_title, url=competition_url, icon_url=competition_data[4])
                        emoji = None
                        if match.get_coach_home().get_id() is not None and match.get_coach_away().get_id() is not None:
                            home_odd, draw_odd, away_odd = Statistics.get_contest_odds(match, platform_id)
                            embed.set_footer(text="🏠 {} - 🇽 {} - 🚌 {}".format(home_odd, draw_odd, away_odd))
                            rsrc = discord_resources.Resources()
                            emoji = [rsrc.get_calendar_emoji(), "🏠", "🇽", "🚌"]
                        msg = await utils.send_custom_message(channel, embed=embed, emoji=emoji)

                        contest_db.add_or_update_contest(match, platform_id, competition.get_rounds_count(), competition_data[5], channel.id, msg.id)
