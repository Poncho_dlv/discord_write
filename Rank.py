#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyshorteners

from discord_resources.Resources import Resources
from discord_resources.Utilities import DiscordUtilities
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator


class Rank:

    def __init__(self, client):
        self.__client = client

    async def write(self, ctx, standing, point_system, competition_data):
        channel = ctx.message.channel
        utils = DiscordUtilities(self.__client)
        standing = standing[:20]
        settings = DiscordGuildSettings(f"{ctx.guild.id}")
        lang = settings.get_language()

        team_label = Translator.tr("#_rank_writer.team", lang)
        points_label = Translator.tr("#_rank_writer.points", lang)
        record_label = Translator.tr("#_rank_writer.record", lang)
        tdd_label = Translator.tr("#_rank_writer.tdd", lang)
        casd_label = Translator.tr("#_rank_writer.casd", lang)
        coach_label = Translator.tr("#_rank_writer.coach", lang)
        race_label = Translator.tr("#_rank_writer.race", lang)
        rank_label = Translator.tr("#_rank_writer.rank", lang)
        ai_label = Translator.tr("#_rank_writer.a_i", lang)
        ranking = "```"

        if competition_data.get("standing_source") == "europen2_standing":
            # /* Points rule: {} */
            sorting_label = Translator.tr("#_rank_writer.point_rule", lang).format(point_system)
            template = "{:>2} {:<40} {:<3} {:<6} {:>4} {:>3} {:>3}\n"
            ranking += template.format("#", team_label, points_label, record_label, "IndP", "TCP", "CW3")
            rank = 1
            for team in standing:
                result = "{}-{}-{}".format(team["win"], team["draw"], team["loss"])
                ranking += template.format(rank, team.get("team_name", "")[:35], team["points"], result, team["individual_points"], team["external_tie_breaker"], team["coleman_waldorf_3"])
                rank += 1
        elif competition_data.get("standing_source") == "europen_standing":
            # /* Points rule: {} */
            sorting_label = Translator.tr("#_rank_writer.point_rule", lang).format(point_system)
            template = "{:>2} {:<30} {:>4} {:<6} {:>3} {:>3}\n"
            ranking += template.format("#", team_label, points_label, record_label, tdd_label, casd_label)
            rank = 1
            for team in standing:
                result = "{}-{}-{}".format(team["win"], team["draw"], team["loss"])
                ranking += template.format(rank, team.get("team_name", "")[:30], team["points"], result, team["tdd"], team["casualties_difference"])
                rank += 1
        elif point_system == "rank":
            sorting_label = "Sorting by rank"
            template = "{:>2} {:<14} {:<20} {:<10} {:<4} {:>6}\n"
            ranking += template.format("#", coach_label, team_label, race_label, rank_label, record_label)
            rank = 1
            for team in standing:
                coach_name = team.get("coach_name")
                if coach_name is None:
                    coach_name = ai_label

                result = "{}-{}-{}".format(team["win"], team["draw"], team["loss"])
                ranking += template.format(rank, coach_name[:14], team.get("team_name", "")[:20], team.get("race", "")[:10], team["sort"], result)
                rank += 1
        elif point_system == "default_bb2":
            sorting_label = "Blood Bowl 2 ranking"
            template = "{:>2} {:<14} {:<20} {:<10} {:<4} {:<6}\n"
            ranking += template.format("#", coach_label, team_label, race_label, points_label, record_label)
            rank = 1
            for team in standing:
                coach_name = team.get("coach_name")
                if coach_name is None:
                    coach_name = ai_label

                result = "{}-{}-{}".format(team["win"], team["draw"], team["loss"])
                ranking += template.format(rank, coach_name[:14], team.get("team_name", "")[:20], team.get("race", "")[:10], team["points"], result)
                rank += 1
        else:
            sorting_label = Translator.tr("#_rank_writer.point_rule", lang).format(point_system)
            template = "{:>2} {:<14} {:<20} {:<10} {:<4} {:<6} {:>3}\n"
            ranking += template.format("#", coach_label, team_label, race_label, points_label, record_label, tdd_label)
            rank = 1
            for team in standing:
                coach_name = team.get("coach_name")
                if coach_name is None:
                    coach_name = ai_label

                result = "{}-{}-{}".format(team["win"], team["draw"], team["loss"])
                ranking += template.format(rank, coach_name[:14], team.get("team_name", "")[:20], team.get("race", "")[:10], team["points"], result, team["tdd"])
                rank += 1
        ranking += "```"

        if competition_data.get("logo"):
            logo = competition_data.get("logo")
        else:
            logo = Resources.get_blood_bowl_2_emoji()

        title = "{} **{}** {} - *{}*\n".format(logo, competition_data.get("name"), logo, sorting_label)
        output = title + ranking
        try:
            s = pyshorteners.Shortener(timeout=4)
            url = s.dagd.short("https://spike.ovh/competition?competition={}&platform={}".format(competition_data.get("id"), competition_data.get("platform_id")))
        except:
            url = "https://spike.ovh/competition?competition={}&platform={}".format(competition_data.get("id"), competition_data.get("platform_id"))
        # /* \nFull standing: {} */
        output += Translator.tr("#_rank_writer.standing_link", lang).format(url)
        await utils.send_custom_message(channel, output)
