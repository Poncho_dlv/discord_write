#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord

import discord_resources
from discord_resources.Utilities import DiscordUtilities
from spike_database.BB2Resources import BB2Resources
from spike_database.Bounty import Bounty
from spike_database.ResourcesRequest import ResourcesRequest
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator


class Team:

    def __init__(self, client, channel):
        self.__client = client
        self.__channel = channel

    @staticmethod
    def format_num(num):
        num = float("{:.3g}".format(num))
        magnitude = 0
        while abs(num) >= 1000:
            magnitude += 1
            num /= 1000.0
        return "{}{}".format("{:f}".format(num).rstrip("0").rstrip("."), ["", "K", "M", "B", "T"][magnitude])

    async def write_team(self, ctx, team, lang, platform, display_attribute, display_tv, display_id, display_bnty):
        utils = DiscordUtilities(self.__client)
        db = ResourcesRequest()
        platform_id = db.get_platform_id(platform)
        platform_data = db.get_platform(platform_id)
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        bnty_db = Bounty()
        rsrc = discord_resources.Resources()

        title = "{} {}".format(platform_data[2], team.get_name())
        url = "https://spike.ovh/team?team_id={}&platform={}".format(team.get_id(), platform_id)
        if display_id:
            title += " - id: {}".format(team.get_id())
        coach_name = team.get_coach().get_name()

        embed = discord.Embed(title=title, description="", color=0x992d22, url=url)
        img_url = db.get_team_emoji_url(team.get_logo())
        embed.set_thumbnail(url=img_url)

        description = "```"
        # /* TV: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.team_value", settings.get_language()), team.get_team_value())
        description += "\n"
        # /* Fan factor: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.fan_factor", settings.get_language()), team.get_popularity())
        description += "\n"
        # /* Cash: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.cash", settings.get_language()), self.format_num(team.get_cash()))
        description += "\n"
        # /* Cheerleaders: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.cheerleaders", settings.get_language()), team.get_nb_cheerleaders())
        description += "\n"
        # /* Apo: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.apothecary", settings.get_language()), team.get_apothecary())
        description += "\n"
        # /* RR: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.reroll", settings.get_language()), team.get_nb_rerolls())
        description += "\n"
        # /* Assistant: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.assistant:", settings.get_language()), team.get_nb_assistant())
        description += "\n"
        # /* Nb players: */
        description += "{:<15}{}".format(Translator.tr("#_team_writer.nb_players:", settings.get_language()), team.get_nb_player())
        description += "```"

        embed.add_field(name=coach_name, value=description, inline=False)
        embed.set_footer(text="Team id: {} - {}".format(team.get_id(), platform))

        for player in team.get_players():
            player_type = db.get_player_type_translation(player.get_type().split("_")[-1], lang)

            title = "{} - {} {}/{} - *{}*".format(player.get_number(), player.get_name(), player.get_spp(), player.get_next_lvl(), player_type)

            if display_bnty:
                bnty_data = bnty_db.get_bounty_data(player.get_id(), platform_id)
                if bnty_data is not None and len(bnty_data.get("issuers", [])) > 0 and (ctx.guild is None or utils.display_bounty(bnty_data, ctx.guild.id)):
                    is_bountied = True
                else:
                    is_bountied = False
            else:
                is_bountied = False

            if is_bountied:
                title += " {}".format(rsrc.get_bounty_emoji())

            if display_attribute:
                title += "  [{}]".format(player.get_attributes())

            if display_tv:
                title += " - {}K".format(player.get_player_value())

            if display_id:
                title += " - id: {}".format(player.get_id())

            field_value = self.get_player_skills(player)
            field_value += player.get_casualties_state()
            if field_value == "":
                field_value += "\u200b"  # Invisible space

            embed.add_field(name=title, value=field_value, inline=False)

        if display_bnty:
            bnty_emoji = discord.utils.get(self.__client.emojis, name="bnty")
        else:
            bnty_emoji = None
        await utils.send_custom_message(self.__channel, embed=embed, emoji=bnty_emoji)

    @staticmethod
    def get_player_skills(player):
        list_of_skills = []
        formatted_skills = ""
        db = ResourcesRequest()
        bb_db = BB2Resources()
        player_id = bb_db.get_player_id(player.get_type_id())
        skills_id = bb_db.get_skills_id(player_id)
        for skill_id in skills_id:
            skill_name = bb_db.get_skills_name(skill_id[0])
            list_of_skills.append(db.get_skill_emoji(skill_name))

        list_of_skills.extend(player.get_skills_emoji())

        for i in range(len(list_of_skills)):
            formatted_skills += list_of_skills[i]
        return formatted_skills
