#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

import discord
from pytz import timezone

import discord_resources
from __init__ import spike_logger
from discord_resources import Resources
from discord_resources.Utilities import DiscordUtilities
from spike_database.Bounty import Bounty
from spike_database.Competitions import Competitions
from spike_database.DiscordGuild import DiscordGuild
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.SpikeAPI.StatisticsAPI import StatisticsAPI
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from spike_utilities.Utilities import Utilities


class Match:

    def __init__(self, client, server_id=None, emo_list=None):
        self.client = client
        if server_id is not None:
            self.db = DiscordGuild(server_id)
        else:
            self.db = None

        self.server_id = server_id

        if emo_list is not None:
            self.emo_list = emo_list
        else:
            self.emo_list = client.emojis

        self.match_overview = "https://spike.ovh/match?match_uuid={}"
        self.coach_link = "https://spike.ovh/coach?coach_id={}&platform_id={}"

    # Write matches result in discord
    async def write_match_result(self, matches_to_report, compact_report, display_ip):
        for match in matches_to_report:
            id_player1 = match.get_coach_home().get_id()
            id_player2 = match.get_coach_away().get_id()
            if id_player1 > 0 and id_player2 > 0:  # Remove AI
                spike_logger.info("Write match: {}".format(match.get_uuid()))
                try:
                    await self.write_data(match, compact_report, display_ip, update_rank=True)
                    self.db.save_match(match.get_uuid())
                except Exception as e:
                    spike_logger.error(f"write_match_result :: match {match.get_uuid()} :: error: {e}")
            else:
                spike_logger.info(f"Ignore match: {match.get_uuid()}")

    async def write_data(self, match, compact_report, display_ip, channel=None, allow_unknown_competition=False, update_rank=False):
        rsrc = discord_resources.Resources()

        # Check for admin games and don't post report
        # Admin result match have started time == finished time
        if match.get_start_datetime() == match.get_finish_datetime():
            # Still return True so that the match will be saved to the db
            return

        settings = DiscordGuildSettings(self.server_id)
        db = ResourcesRequest()

        match_overview = self.match_overview.format(match.get_uuid())
        platform_id = match.get_platform_id()
        competition_link = f"https://spike.ovh/competition?competition_id={match.get_competition_id()}&platform_id={match.get_platform_id()}"
        competition_data = None

        if channel is None or not isinstance(channel, discord.abc.PrivateChannel):
            competition_id = self.db.get_competition_id(match.get_competition_name(), platform_id)
            if competition_id is None and allow_unknown_competition:
                competition_data = DiscordUtilities.get_default_competition_data(match.get_league_name(), match.get_competition_name(),
                                                                                 match.get_platform(), channel)
            else:
                competition_data = self.db.get_competition_data(competition_id)
        elif isinstance(channel, discord.abc.PrivateChannel):
            competition_data = DiscordUtilities.get_default_competition_data(match.get_league_name(), match.get_competition_name(),
                                                                             match.get_platform(), channel)

        if competition_data is not None:
            # Data Player 1
            player1 = match.get_coach_home().get_name()
            player1_link = self.coach_link.format(match.get_coach_home().get_id(), platform_id)
            team1 = match.get_team_home().get_name()
            logo_team1 = db.get_team_emoji(match.get_team_home().get_logo())

            if logo_team1 is None:
                logo_team1 = rsrc.get_blood_bowl_2_emoji()

            # Data Player 2
            player2 = match.get_coach_away().get_name()
            player2_link = self.coach_link.format(match.get_coach_away().get_id(), platform_id)
            team2 = match.get_team_away().get_name()
            logo_team2 = db.get_team_emoji(match.get_team_away().get_logo())

            if logo_team2 is None:
                logo_team2 = rsrc.get_blood_bowl_2_emoji()

            # Format data
            match_title = f"{team1} vs {team2}"
            description = f"\n[{player1}]({player1_link}) {logo_team1} vs {logo_team2} [{player2}]({player2_link})"

            default_tz = timezone("UTC")
            datetime_obj = datetime.datetime.strptime(match.get_start_datetime(), "%Y-%m-%d %H:%M:%S")
            datetime_obj = datetime_obj.replace(tzinfo=default_tz)
            local_timezone = timezone(settings.get_timezone())
            date_format = "%d/%m/%Y"
            time_format = "%H:%M"
            # /* Played {date} at {time} */
            played_label = Translator.tr("#_match_writer.footer", settings.get_language()).format(date=date_format, time=time_format)
            footer = datetime_obj.astimezone(local_timezone).strftime(f"{played_label} {settings.get_timezone()} - id: {match.get_uuid()}")

            if competition_data[4] is None:
                thumbnail = "https://spike.ovh/static/spike_resources/img/logos/Logo_Neutre_04.png"
            else:
                thumbnail = competition_data[4]

            # Display result
            if channel is None:
                output_channel = self.client.get_channel(competition_data[3])
                if output_channel is None:
                    spike_logger.error(f"write_data :: fetch output_channel for :: {match.get_uuid()}")
                    output_channel = await self.client.fetch_channel(competition_data[3])
            else:
                output_channel = channel

            if output_channel is None:
                # disable competition report
                if self.db is not None and self.server_id is not None:
                    spike_logger.error(f"Guild: {self.server_id} :: remove competition {competition_data[2]}")
                    self.db.remove_competition(competition_data[2], competition_data[6])
            else:
                stat_embed = discord.Embed(title=match_title, url=match_overview, description=description, color=0x992d22)
                stat_embed.set_author(name=match.get_competition_name(), url=competition_link, icon_url=thumbnail)
                stat_embed.set_thumbnail(url=thumbnail)
                stat_embed.set_footer(text=footer)

                stats_title = Translator.tr("#_common_command.statistics_title", settings.get_language())
                # Dirty hack to fix a minimum width to stat tab
                for i in range(0, 50):
                    stats_title += "\u200b "

                stat_embed.add_field(name=stats_title, value=self.generate_game_stat(match, settings.get_language()))

                if display_ip:
                    lang = settings.get_language()
                    impact_players = match.get_impact_player()[:3]
                    if len(impact_players) > 0 and impact_players[0][0] > 0:
                        # /* {logo} Impact Player */
                        impact_player_title = Translator.tr("#_match_writer.impact_player_title", lang).format(logo=rsrc.get_mvp_emoji())
                        impact_report = ""
                        trophy = (rsrc.get_gold_trophy(), rsrc.get_silver_trophy(), rsrc.get_bronze_trophy())

                        for i in range(0, 3):
                            player_name = impact_players[i][1].get_name(lang)

                            if player_name.strip() == "":
                                player_name = impact_players[i][1].get_number()

                            if match.get_team_home().get_player(impact_players[i][1].get_id(), player_name, lang) is not None:
                                team_logo = logo_team1
                            else:
                                team_logo = logo_team2
                            impact_report += "\n\n{} {} **{:<35}** {} pts".format(trophy[i], team_logo, player_name, impact_players[i][0])
                            impact_report += "\n{}".format(impact_players[i][1].get_impact_player_stats())

                        if not Utilities.is_cabal_vision(match.get_league_id(), platform_id):
                            impact_report += "\n\n[Hall of fame](https://spike.ovh/competition_stat?competition={}&platform={})".format(
                                match.get_competition_id(), platform_id)
                        stat_embed.add_field(name=impact_player_title, value=impact_report)

                if compact_report:
                    detail_emoji_obj = discord.utils.get(self.emo_list, name="More")
                    msg = await DiscordUtilities.send_custom_message(output_channel, embed=stat_embed, emoji=detail_emoji_obj, raise_error=False,
                                                                     send_typing=False)
                else:
                    msg = await DiscordUtilities.send_custom_message(output_channel, embed=stat_embed, raise_error=False, send_typing=False)

                if msg is None and isinstance(msg, discord.Message):
                    spike_logger.error("write_data :: GENERAL MESSAGE NONE :: {}".format(match.get_uuid()))

                if compact_report is False:
                    msg_home, msg_away = await self.generate_teams_report(match, output_channel, platform_id, update_rank)
                    if msg_home is None and isinstance(msg, discord.Message):
                        spike_logger.error("write_data :: HOME MESSAGE NONE :: {}".format(match.get_uuid()))
                    if msg_away is None and isinstance(msg, discord.Message):
                        spike_logger.error("write_data :: AWAY MESSAGE NONE :: {}".format(match.get_uuid()))
        else:
            spike_logger.error("write_data :: competition_data is None :: {}".format(match.get_uuid()))

    # Generate embed message
    def generate_game_stat(self, match, lang):
        game_stat = "```ml\n"
        # /* TD */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.td", lang), match.get_score_home(), match.get_score_away(),
                                             hide_null=False)
        # /* TV */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.tv", lang), match.get_team_home().get_team_value(),
                                             match.get_team_away().get_team_value())
        # /* Blk */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.block", lang), match.get_inflicted_blocks_home(),
                                             match.get_inflicted_blocks_away())
        # /* AvBr */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.armor_break", lang), match.get_inflicted_AvBr_home(),
                                             match.get_inflicted_AvBr_away())
        # /* Ko */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.ko", lang), match.get_inflicted_ko_home(), match.get_inflicted_ko_away())
        # /* Cas */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.casualties", lang), match.get_inflicted_casualties_home(),
                                             match.get_inflicted_casualties_away())
        # /* Death */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.death", lang), match.get_inflicted_dead_home(),
                                             match.get_inflicted_dead_away())
        # /* Surf */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.surf", lang), match.get_inflicted_surf_home(),
                                             match.get_inflicted_surf_away())
        # /* Pass */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.pass", lang), match.get_inflicted_passes_home(),
                                             match.get_inflicted_passes_away())
        # /* Int */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.interception", lang), match.get_inflicted_interceptions_home(),
                                             match.get_inflicted_interceptions_away())
        # /* Pos.% */
        game_stat += self.generate_stat_line(Translator.tr("#_match_writer.possession", lang), match.get_possession_home(),
                                             match.get_possession_away(), True)
        game_stat += "```"
        return game_stat

    @staticmethod
    def generate_stat_line(label, value1, value2, last=False, hide_null=True):
        # To save space, don't print a stat line if both values are zero
        if hide_null and value1 == value2 == 0:
            return ""

        line = "{:<10}".format(label)
        line += "{:>4}".format(value1)
        line += " "
        line += "{:<4}".format(value2)
        if not last:
            line += "\n"
        return line

    # Generate embed message
    async def generate_teams_report(self, match, output_channel, platform_id, update_rank=False):
        competition_db = Competitions()
        standing = competition_db.get_standing(match.get_competition_id(), platform_id)
        home_rank = {"win": 0, "draw": 0, "loss": 0}
        away_rank = {"win": 0, "draw": 0, "loss": 0}
        if standing is not None and standing.get("standing") is not None:
            for team in standing.get("standing", []):
                if team.get("team_id") == match.get_team_home().get_id():
                    home_rank = team
                elif team.get("team_id") == match.get_team_away().get_id():
                    away_rank = team
            if update_rank:
                if match.get_winner_team() is not None:
                    if match.get_winner_team().get_id() == match.get_team_home().get_id():
                        home_rank["win"] += 1
                        away_rank["loss"] += 1
                    else:
                        home_rank["loss"] += 1
                        away_rank["win"] += 1
                else:
                    home_rank["draw"] += 1
                    away_rank["draw"] += 1
        msg_home = await self.generate_team_report(match.get_team_home(), output_channel, platform_id, home_rank)
        msg_away = await self.generate_team_report(match.get_team_away(), output_channel, platform_id, away_rank)
        return msg_home, msg_away

    async def generate_team_report(self, team, output_channel, platform_id, rank=None):
        settings = DiscordGuildSettings(self.server_id)
        lang = settings.get_language()
        utils = DiscordUtilities(self.client)
        rsrc_db = ResourcesRequest()
        bounty_db = Bounty()
        rsrc = discord_resources.Resources()
        bounty_emo = rsrc.get_bounty_emoji()
        url = f"https://spike.ovh/team?team_id={team.get_id()}&platform={platform_id}"

        embed = discord.Embed(title=team.get_name(), description="\u200b ", color=0x992d22, url=url)
        img_url = rsrc_db.get_team_emoji_url(team.get_logo())

        if img_url is None:
            img_url = "https://spike.ovh/static/spike_resources/img/logos/Logo_Neutre_04.png"
        embed.set_thumbnail(url=img_url)
        embed.set_footer(text=f"Team id: {team.get_id()} - {rsrc_db.get_platform_label(platform_id)} - Coach id: {team.get_coach().get_id()} ")

        team_info = "Race: {}\n".format(team.get_race_label())
        total_spp = team.get_total_spp() + team.get_earned_spp()
        team_info += "Total Spp: {} ({})\n".format(total_spp, team.get_earned_spp())
        if rank is not None:
            team_info += "Results: {}-{}-{}".format(rank.get("win"), rank.get("draw"), rank.get("loss"))

        team_info += "\n\u200b"
        # /* Team info */
        embed.add_field(name=Translator.tr("#_match_writer.team_info_title", lang), value=team_info, inline=True)

        coach_stats = StatisticsAPI.get_win_rate(team.get_coach().get_id(), platform_id, race_id=team.get_race_id())
        if coach_stats is not None:
            win_rate = coach_stats["win_rate"]
            win = coach_stats["nb_win"]
            draw = coach_stats["nb_draw"]
            loss = coach_stats["nb_loss"]
            nb_match = coach_stats["nb_match"]
            coach_info = "[{}](https://spike.ovh/coach?coach_id={}&platform_id={})".format(team.get_coach().get_name(), team.get_coach().get_id(),
                                                                                           platform_id)
            coach_info += "\n{}: {}%".format(team.get_race(), win_rate)
            games_label = Translator.tr("#_common_command.games_label", lang)
            coach_info += "\n{}: {}-{}-{} ({} {})".format(team.get_race(), win, draw, loss, nb_match, games_label)

            # /* Coach info */
            embed.add_field(name=Translator.tr("#_match_writer.coach_info", lang), value=coach_info, inline=True)
        else:
            spike_logger.debug("Unable to get win rate for: {} - {} - {}".format(team.get_coach().get_name(), platform_id, team.get_race()))

        inline = False
        for player in team.get_players():
            if not player.is_champion():
                if player.is_champion():
                    player_name = "{} {}".format(Resources.get_star_player_emoji(), player.get_name(lang))
                else:
                    player_name = "{} {}".format(Resources.get_lvl_emoji(player.get_level()), player.get_name(lang))
                    if player.is_levelling_up():
                        player_name += " {}".format(Resources.get_lvl_up_emoji())

                if player.get_spp_gain() > 0 or player.is_injured():
                    if settings.get_enable_bounty():
                        bounty_data = bounty_db.get_bounty_data(player.get_id(), platform_id)
                        if bounty_data is not None and len(bounty_data.get("issuers", [])) > 0 and utils.display_bounty(bounty_data, self.server_id):
                            is_bounty = True
                        else:
                            is_bounty = False
                    else:
                        is_bounty = False

                    if is_bounty:
                        title = "{}{:<35}".format(player_name, bounty_emo)
                    else:
                        title = "{:<35}".format(player_name)

                    if not player.is_champion():
                        output_text = "{}".format(rsrc_db.get_player_type_translation(player.get_type().split("_")[-1], lang))

                        spp = player.get_spp() + player.get_spp_gain()
                        output_text += " {}/{}".format(str(spp), str(player.get_next_lvl()))
                        if player.get_spp_gain() > 0:
                            output_text += " (+{})".format(str(player.get_spp_gain()))
                        output_text += "\n"
                    else:
                        output_text = ""

                    skills_and_cases = player.get_skills_emoji()

                    if player.is_injured():
                        skills_and_cases.extend(player.get_casualties_sustained())

                    for index in range(0, len(skills_and_cases)):
                        if index % 4 == 0 and index != 0:
                            output_text += "\n"
                        output_text += skills_and_cases[index]
                    output_text += "\n\u200b "

                    embed.add_field(name=title, value=output_text, inline=inline)
                    inline = True

        if settings.get_enable_bounty():
            bounty_emo_obj = discord.utils.get(self.emo_list, name="bnty")
        else:
            bounty_emo_obj = None
        msg = await DiscordUtilities.send_custom_message(output_channel, embed=embed, emoji=bounty_emo_obj, send_typing=False)
        return msg
